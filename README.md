# Spice Rack
It's a 3D printable spice rack.  Really.

More info on the 'why' at https://snowgoons.ro/posts/2020-08-11-3d-design-holiday/

This 3D printable rack holds 15 'standard' spice jars, or up to 30 of the
curious rectangular ~15g spice pots from my local Mega Image/Delhaize
supermarket.

### Author
Copyright (c) 2020 by Tim Walls- https://snowgoons.ro/

Licensed under a CC BY-NC-SA license - see LICENSE.md for details.


## What's in the box
There are a number of components that go to make up the rack:

| File            | Description |
| --------------- | ----------- |
| Base.stl        | This is the based upon which the rest of the components sit.  You'll need one. |
| Rack Tier.stl   | These sit on the base.  Each tier can have 6 holders hanging from it.  Make one (to carry 6 holders) or two (to carry 12). |
| Top Tier.stl    | The top tier is slightly different, being more slender (it carries 3 holders), and not having the holes to insert additional tiers into.  Make one. |
| Mp Holder.stl   | The "multipurpose holder".  It should be good for round spice jars up to 45mm diameter, or square jars up to 40x40mm.  This covers all the common spice jars in my collection. Make as many as you need to hang off the rack tiers! |
| Twin Holder.stl | A holder for Delhaize/Mega Image supermarkets' small ~15g spice pots; these are a slightly unusual 21mm x 50mm rectangular size.  The Twin Holder can carry two of them side by side.  Again, make as many as you wish to hang. |
| Hook.stl        | These hooks slide into the back of the holders, and let you hook them on the rack.  Print as many as you have holders to hang. |

## Assembly
* Place the base on the table.
* Insert the legs of the first Rack Tier into the holes in the base.  It should 
  slide in easily.
* Insert a second Rack Tier into the holes in the top of the first rack tier,
  if you want.
* Insert the Top Tier into the holes on the last tier, to 'finish off' the
  basic tree.
* Into each of the rectangular holes on the tiers, insert the 'little end' of
  a hook into the hole, so that the longer flat side hangs over the edge at an
  angle of roughly 15 degrees off vertical.
* Now you can slide your holders onto the hooks...
* ...and finally, add spice jars!

Everything should sit together fine under its own gravity.  But you could
choose to use a little glue to fix all the parts together permanently if you
wish (if you do choose to, I suggest glue to the hooks to their corresponding
holders but NOT to the rack tiers, to give yourself flexibility to rearrange
things if you so wish.)

## Printing Tips
There are some overhangs in the parts, but all are printable without needing
supports.  I do recommend a brim, but in generaly I find that works well for
anything on my printer.

Otherwise, it all prints fine on my AnyCubic Mega-X using the following
settings:

* Wall thickness: 1.2mm
* Infill: 20%, I use gyroid
* Adhesion: Brim
* Supports: No
* Print rate: 60mm/s
* Material: PLA

